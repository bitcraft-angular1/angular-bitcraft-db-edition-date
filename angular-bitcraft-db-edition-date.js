(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

var template = '' +
'<!--suppress ALL -->' +
'<div ng-if="!$ctrl.edit">{{$ctrl.value | date:\'yyyy/MM/dd\'}}</div>' +
'<div ng-if="$ctrl.edit">' +
'    <div class="input-group">' +
'        <button class="form-control">{{$ctrl.value | date:\'yyyy/MM/dd\'}}</button>' +
'        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>' +
'    </div>' +
'    <datetimepicker data-ng-model="$ctrl.value" data-datetimepicker-config="{ minView:\'day\' }"></datetimepicker>' +
'</div>' +
'';


//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-date')
    .component('bitcraftDbEditionDate', {
        template: template,
        bindings: {
            value: '=',
            edit: '<'
        }
    });

},{}],2:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

angular.module('bitcraft-db-edition-date', ['ui.bootstrap.datetimepicker']);

},{}],3:[function(require,module,exports){
/**
 * Created by richard on 19/10/16.
 */

'use strict';
require('./db-edition-date.module.js');
require('./db-edition-date.component.js');

},{"./db-edition-date.component.js":1,"./db-edition-date.module.js":2}]},{},[3]);
