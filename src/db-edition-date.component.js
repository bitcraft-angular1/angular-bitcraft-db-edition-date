/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-date')
    .component('bitcraftDbEditionDate', {
        templateUrl: './js/db-edition-date/db-edition-date.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
